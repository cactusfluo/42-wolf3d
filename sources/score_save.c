#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		save_new_scores(int32_t file, t_score *scores)
{
	uint8_t		i;

	i = 0;
	while (i < SCORE_MAX_SAVE && scores[i].score)
	{
		write(file, scores[i].name, SCORE_NAME_MAX);
		write(file, &(scores[i].score), 4);
		i += 1;
	}
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			score_save(t_wolf *wolf, t_score *scores, uint8_t score_index,
				char *player_name)
{
	char		*save_file_path;
	int32_t		file;

	if (!(save_file_path = ct_strjoin(wolf->race_path, "-save")))
		ct_exit_fail("Not enough memory to save player score.");
	if ((file = open(save_file_path, O_WRONLY | O_CREAT | O_TRUNC, 0666)) < 0)
		ct_exit_fail("Cannot write on score file.");
	free(save_file_path);
	scores[score_index].score = wolf->score;
	ct_memcpy(scores[score_index].name, player_name, SCORE_NAME_MAX);
	save_new_scores(file, scores);
	close(file);
}
