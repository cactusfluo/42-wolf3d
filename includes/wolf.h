/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: shuertas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/05 14:37:43 by shuertas          #+#    #+#             */
/*   Updated: 2018/04/17 11:35:51 by shuertas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

/*
********************************************************************************
** INCLUDES ********************************************************************
********************************************************************************
*/

# include <SDL2/SDL.h>
# include <fcntl.h>
# include <pthread.h>
# include "libct.h"

/*
********************************************************************************
** DEFINES *********************************************************************
********************************************************************************
*/

/*
**************************************************
** ENV *******************************************
**************************************************
*/
# define PATH_DEFAULT_RACE		"./ressources/races/mega"
# define PATH_PANEL				"./ressources/panel.bmp"
# define PATH_LIFEBAR			"./ressources/lifebar.bmp"
# define PATH_POWERBAR			"./ressources/powerbar.bmp"
# define PATH_POWERBARMAX		"./ressources/powerbarmax.bmp"
# define PATH_DIGITS			"./ressources/digits.bmp"
# define PATH_SPACESHIP			"./ressources/spaceship.bmp"
# define PATH_ALPHABET			"./ressources/alpha.bmp"
# define PATH_CURSOR			"./ressources/cursor.bmp"

/*
**************************************************
** CONFIG ****************************************
**************************************************
*/
# define PI						3.14159265358979
# define WINDOW_W				1600
# define WINDOW_H				500
# define DEFAULT_CAM_WIDTH		1.7 // 1.7
# define COLOR_NUMBER			5
# define ANIM_DELAY				0.01666 // 0.0833
# define ANIM_MAX				60
# define GRID_WIDTH				0.02
# define LIFEBAR_UNIT_W			10
# define LIFEBAR_W				280
# define LIFEBAR_X				160
# define LIFEBAR_Y				450
# define POWERBAR_UNIT_W		10
# define POWERBAR_W				280
# define POWERBAR_X				160
# define POWERBAR_Y				470
# define SPEED_X				20
# define SPEED_Y				380
# define SPACESHIP_ROT_MIN		80

/*
**************************************************
** GAMEPLAY **************************************
**************************************************
*/
# define GAME_LIFE_DEFAULT		200
# define GAME_POWER_MAX			200
# define GAME_TILE_SOLID		5
# define GAME_TILE_END			25
# define GAME_PL_OFFSET			0.1
# define GAME_PL_SIZE			0.2
# define GAME_ACC				40 // 10
# define GAME_SLOW				5 // 2
# define GAME_MIN_SPEED			0
# define GAME_MAX_SPEED			30 // 15
# define GAME_ANGLE_ACC			600
# define GAME_ANGLE_SLOW		150
# define GAME_ANGLE_MIN_SPEED	1
# define GAME_ANGLE_MAX_SPEED	200 // 150
# define GAME_CAMSPEED_COEF		3	// -1
# define GAME_SPEED_UP_TIME		1
# define GAME_SPEED_UP_COEF		1.5
# define GAME_CHOCK_MIN			10
# define GAME_POWER_COEF		1
# define GAME_POWER_TIME		5

# define MINIMAP_X				686
# define MINIMAP_Y				384
# define MINIMAP_W				228
# define MINIMAP_H				90

/*
**************************************************
** SCORE *****************************************
**************************************************
*/
# define SCORE_NAME_MAX			6
# define SCORE_NAME_DIST		40
# define SCORE_NAME_X			(WINDOW_W / 2 - (SCORE_NAME_MAX * \
								SCORE_NAME_DIST) / 2)
# define SCORE_NAME_Y			250
# define SCORE_CURSOR_Y			300
# define SCORE_MAX_SAVE			10
# define SCORE_X				SCORE_NAME_X + SCORE_NAME_DIST * \
								(SCORE_NAME_MAX + 1)
# define SCORE_Y				SCORE_NAME_Y
# define SCORES_X				20
# define SCORES_Y				-4
# define SCORES_OFFSET			50

/*
********************************************************************************
** TYPEDEFS ********************************************************************
********************************************************************************
*/

typedef struct s_score	t_score;
typedef struct s_color	t_color;
typedef struct s_vec	t_vec;
typedef struct s_dvec	t_dvec;
typedef struct s_sdl	t_sdl;
typedef struct s_images	t_images;
typedef struct s_keys	t_keys;
typedef struct s_wolf	t_wolf;

/*
********************************************************************************
** STRUCTURES ******************************************************************
********************************************************************************
*/

struct				s_score
{
	char			name[SCORE_NAME_MAX];
	uint32_t		score;
};

struct				s_color
{
	uint8_t			r;
	uint8_t			g;
	uint8_t			b;
};

struct				s_vec
{
	int32_t			x;
	int32_t			y;
};

struct				s_dvec
{
	double			x;
	double			y;
};

struct				s_sdl
{
	SDL_Window		*win;
	SDL_Renderer	*rend;
};

struct				s_keys
{
	uint8_t			up : 1;
	uint8_t			down : 1;
	uint8_t			left : 1;
	uint8_t			right : 1;
	uint8_t			space : 1;
	uint8_t			enter : 1;
};

struct				s_images
{
	SDL_Texture		*panel;
	SDL_Texture		*lifebar;
	SDL_Texture		*powerbar;
	SDL_Texture		*powerbarmax;
	SDL_Texture		*spaceship;
	SDL_Texture		*digits;
	SDL_Texture		*alpha;
	SDL_Texture		*cursor;
};

struct				s_wolf
{
	t_sdl			sdl;
	t_keys			keys;
	t_images		images;
	char			*race_path;
	uint8_t			**map;
	uint16_t		map_w;
	uint16_t		map_h;
	t_dvec			pos;
	t_vec			pos_map;
	double			angle;
	double			angle_acc;
	double			speed;
	t_dvec			acc;
	t_dvec			sacc;
	t_dvec			rot;
	t_dvec			cam;
	double			cam_width;
	double			speed_up;
	uint8_t			frame;
	double			life;
	double			power_charge;
	double			time;
	uint32_t		score;
};

/*
********************************************************************************
** FUNCTIONS *******************************************************************
********************************************************************************
*/

void		load_map(t_wolf *wolf, char *file_name);
uint8_t		*load_map_line(int32_t file, uint16_t *length);

SDL_Texture	*load_image(t_wolf *wolf, char *path);
void		loop(t_wolf *wolf);
void		handle_movements(t_wolf *wolf, double dt);
void		handle_collisions(t_wolf *wolf);
void		handle_speed_up(t_wolf *wolf, uint8_t type);
void		handle_crash(t_wolf *wolf);
void		handle_power_charge(t_wolf *wolf, double dt);
void		power_activate(t_wolf *wolf);

/*
**************************************************
** DRAWING ***************************************
**************************************************
*/
void		get_color(t_color *color, uint8_t offset, double alteration);
void		set_color(t_wolf *wolf, uint8_t offset);
void		draw_map(t_wolf *wolf);
void		draw_floor(t_wolf *wolf, t_vec map_coords, double dist_wall,
			double wall_x, uint8_t side, uint16_t x);
void		draw_floor_arrow(t_wolf *wolf, t_dvec floor_coords, uint8_t type,
			uint16_t x, uint16_t y);

/*
******************************
** LINE DRAWING **************
******************************
*/
void		draw_line_std(t_wolf *wolf, double dist, uint16_t x, uint8_t type,
			uint8_t side);
void		draw_line_end(t_wolf *wolf, double dist, uint16_t x);
void		draw_line_square(t_wolf *wolf, double dist, uint16_t x,
			double wall_precise);
void		draw_line_rect(t_wolf *wolf, double dist, uint16_t x,
			uint8_t type, uint8_t side, double wall_precise);
void		draw_line_arrow(t_wolf *wolf, double dist, uint16_t x,
			uint8_t type, uint8_t side, double wall_precise);
void		draw_line_speed(t_wolf *wolf, double dist, uint16_t x);

/*
******************************
** PANEL/SPACESHIP  DRAWING **
******************************
*/
void		draw_panel(t_wolf *wolf);
void		draw_lifebar(t_wolf *wolf);
void		draw_powerbar(t_wolf *wolf);
void		draw_speed(t_wolf *wolf);
void		draw_spaceship(t_wolf *wolf);
void		draw_time(t_wolf *wolf);
void		draw_minimap(t_wolf *wolf);

/*
**************************************************
** SCORE *****************************************
**************************************************
*/
void		loop_score(t_wolf *wolf);
void		load_scores(t_wolf *wolf, t_score *scores, uint8_t *new_score);
void		handle_score_events(t_wolf *wolf, char *name, uint8_t *cursor);
void		score_save(t_wolf *wolf, t_score *scores, uint8_t score_index,
			char *player_name);
void		draw_score_name(t_wolf *wolf, char *name, uint8_t cursor);
void		draw_scores(t_wolf *wolf, t_score *scores);
void		draw_new_score(t_wolf *wolf);

#endif
