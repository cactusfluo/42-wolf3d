#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		draw_squared(t_wolf *wolf,
								double height,
								t_color color_base,
								t_color color_square,
								uint16_t x,
								uint16_t type)
{
	int32_t		y;

	y = (WINDOW_H - height) * 0.5;
	SDL_SetRenderDrawColor(wolf->sdl.rend, color_base.r, color_base.g,
	color_base.b, 0xFF);
	SDL_RenderDrawLine(wolf->sdl.rend, x, MAX(0, y), x,
	MIN(WINDOW_H, y + height));
	height *= 0.1 + 0.90 - ((double)((wolf->frame + type * 2) % (ANIM_MAX / 2)) 
	/ (ANIM_MAX / 2)) * 0.90;
	y = (WINDOW_H - height) * 0.5;
	SDL_SetRenderDrawColor(wolf->sdl.rend, color_square.r, color_square.g,
	color_square.b, 0xFF);
	SDL_RenderDrawLine(wolf->sdl.rend, x, y, x, y + height);
}

static void		draw_standard(t_wolf *wolf,
								double height,
								t_color color_base,
								uint16_t x)
{
	uint16_t	y;

	SDL_SetRenderDrawColor(wolf->sdl.rend, color_base.r, color_base.g,
	color_base.b, 0xFF);
	if (height >= WINDOW_H)
		SDL_RenderDrawLine(wolf->sdl.rend, x, 0, x, WINDOW_H);
	else
	{
		y = (WINDOW_H - height) / 2;
		SDL_RenderDrawLine(wolf->sdl.rend, x, y, x, y + height);
	}
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_line_rect(t_wolf *wolf,
								double dist,
								uint16_t x,
								uint8_t type,
								uint8_t side,
								double wall_precise)
{
	double		coef;
	double		height;
	t_color		color_base;
	t_color		color_square;
	uint16_t	min_y;

	coef = (side & 1) ? 0.5 : 1;
	coef = 1;
	get_color(&color_base, 2, coef);
	get_color(&color_square, 3, coef);
	height = WINDOW_H / dist;
	min_y = ABS((WINDOW_H - height) * 0.5);
	if (wall_precise >= 0.45
	&& wall_precise <= 0.55)
		draw_squared(wolf, height, color_base, color_square, x, type);
	else
		draw_standard(wolf, height, color_base, x);
}
