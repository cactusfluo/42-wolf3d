# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: shuertas <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/21 14:10:28 by shuertas          #+#    #+#              #
#    Updated: 2018/04/17 11:24:39 by shuertas         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

C_GREEN 	= \033[32;01m
C_NO 		= \033[0m
C_RED 		= \033[31;01m
C_YELLOW 	= \033[33;01m
C_CYAN		= \033[36;01m
C_PINK		= \033[35;01m
C_BLACK 	= \033[30;01m

################################################################################
##[ SOURCES ]###################################################################
################################################################################

SRCS		= 	./sources/wolf.c \
				./sources/loop.c \
				./sources/load_map.c \
				./sources/load_map_line.c \
				./sources/load_image.c \
				./sources/draw_map.c \
				./sources/draw_line_std.c \
				./sources/draw_line_end.c \
				./sources/draw_line_square.c \
				./sources/draw_line_rect.c \
				./sources/draw_line_arrow.c \
				./sources/draw_line_speed.c \
				./sources/draw_floor.c \
				./sources/draw_floor_arrow.c \
				./sources/get_color.c \
				./sources/set_color.c \
				./sources/movements.c \
				./sources/collisions.c \
				./sources/crash.c \
				./sources/speed_up.c \
				./sources/power_charge.c \
				./sources/power_activate.c \
				./sources/draw_lifebar.c \
				./sources/draw_powerbar.c \
				./sources/draw_speed.c \
				./sources/draw_panel.c \
				./sources/draw_spaceship.c \
				./sources/draw_time.c \
				./sources/draw_minimap.c \
				./sources/draw_score_name.c \
				./sources/draw_scores.c \
				./sources/draw_new_score.c \
				./sources/score_load.c \
				./sources/score_loop.c \
				./sources/score_events.c \
				./sources/score_save.c

OBJS		= $(SRCS:.c=.o)

NAME		= wolf
FLAGS		= -Wall -Wextra -Werror -g
CC			= gcc
INCLUDES	= -I ./includes/ -I ./libct/
LIB			= libct/libct.a
SDL			= -framework SDL2

BIN_NUM		= 0
BIN_MAX		= $(shell echo $(OBJS) | wc -w)

################################################################################
##[ RULES ]#####################################################################
################################################################################

##############################
##[ MAIN RULE ]###############
##############################
all: $(NAME)

##############################
##[ LIBRARY RULE ]############
##############################
$(NAME): $(OBJS)
	@tput cuu1 ; tput cuf 1
	@printf "100"
	@tput cnorm
	@rm -rf .bin_num .bin_num_tmp .bin_state
	@printf "\n$(C_GREEN)SOURCES $(C_BLACK)[$(C_YELLOW)OK$(C_BLACK)]$(C_NO)\n"
	@make -C libct
	@gcc $(FLAGS) $(OBJS) $(LIB) $(SDL) -o $(NAME)
	@printf "$(C_GREEN)WOLF 3D$(C_BLACK)[$(C_YELLOW)DONE$(C_BLACK)]$(C_NO)\n"

##############################
##[ COMPILATION RULE ]########
##############################
%.o: %.c Makefile includes/wolf.h libct/libct.h
	@if [ -z ${INTRO} ]; then \
		tput civis; \
		printf "$(C_CYAN)[$(C_YELLOW)000 $(C_PINK)%%$(C_CYAN)]$(C_YELLOW)\n"; \
	fi
	$(eval INTRO := 1)
	@gcc $(FLAGS) $(INCLUDES) -o $@ -c $< || (tput cnorm && FAIL 2>&-)
	@$(eval BIN_NUM		:= $(shell echo "${BIN_NUM}+1" | bc))
	@$(eval BIN_STATE 	:= $(shell echo "${BIN_NUM}*100/${BIN_MAX}" | bc))
	@tput cuu1 ; tput cuf 1
	@printf "%.3d\n" ${BIN_STATE}

##############################
##[ CLEANING RULES ]##########
##############################
clean:
	@rm -rf $(OBJS)

fclean: clean
	@rm -rf $(NAME)

##############################
##[ RESTART RULES ]###########
##############################
re: fclean
	@make
