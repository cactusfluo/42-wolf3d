#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		print_digit(t_wolf *wolf, uint8_t digit, uint32_t x)
{
	SDL_Rect	image_part;
	SDL_Rect	image_pos;

	image_part.x = digit * 60;
	image_part.y = 0;
	image_part.w = 60;
	image_part.h = 60;
	image_pos.x = x;
	image_pos.y = 10;
	image_pos.w = 30;
	image_pos.h = 30;
	SDL_RenderCopy(wolf->sdl.rend, wolf->images.digits, &image_part,
	&image_pos);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_time(t_wolf *wolf)
{
	char		*string;
	uint8_t		length;
	uint8_t		i;

	string = ct_itoa(wolf->time * 1000);
	length = ct_strlen(string);
	i = 0;
	while (string[i])
	{
		if (i == length - 3)
			print_digit(wolf, 10, 10 + 15 * i);
		if (i >= length - 3)
			print_digit(wolf, (string[i] - '0'), 10 + 15 * (i + 1));
		else
			print_digit(wolf, (string[i] - '0'), 10 + 15 * i);
		i++;
	}
	free(string);
}
