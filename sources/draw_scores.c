#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static inline void	draw_score(t_wolf *wolf, uint32_t score, uint32_t y)
{
	SDL_Rect		img_pos;
	SDL_Rect		img_part;
	uint32_t		x;
	char			*string;

	string = ct_itoa(score);
	img_part.y = 0;
	img_part.w = 60;
	img_part.h = 60;
	img_pos.y = y;
	img_pos.w = 60;
	img_pos.h = 60;
	x = 0;
	while (string[x])
	{
		img_pos.x = SCORES_X + SCORE_NAME_DIST * (SCORE_NAME_MAX + 1)
		+ 30 * x;
		img_part.x = (string[x] - '0') * 60;
		SDL_RenderCopy(wolf->sdl.rend, wolf->images.digits, &img_part,
		&img_pos);
		++x;
	}
	free(string);
}

static inline void	draw_name(t_wolf *wolf, char *name, uint32_t y)
{
	SDL_Rect	img_pos;
	SDL_Rect	img_part;
	uint32_t		i;

	img_pos.y = y;
	img_pos.w = 60;
	img_pos.h = 60;
	img_part.y = 0;
	img_part.w = 60;
	img_part.h = 60;
	i = 0;
	while (i < SCORE_NAME_MAX && name[i])
	{
		img_pos.x = SCORES_X + i * SCORE_NAME_DIST;
		img_part.x = ((ct_isalpha(name[i])) ? name[i] - 'a' : 26) * 60;
		SDL_RenderCopy(wolf->sdl.rend, wolf->images.alpha, &img_part,
		&img_pos);
		i += 1;
	}
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_scores(t_wolf *wolf, t_score *scores)
{
	uint32_t	y;
	uint8_t		i;

	i = 0;
	while (i < SCORE_MAX_SAVE && scores[i].score)
	{
		y = SCORES_Y + i * SCORES_OFFSET;
		draw_name(wolf, scores[i].name, y);
		draw_score(wolf, scores[i].score, y);
		i += 1;
	}
}
