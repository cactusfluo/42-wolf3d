#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		draw_squared(t_wolf *wolf,
								double height,
								t_color color_base,
								t_color color_square,
								uint16_t x,
								double square_size)
{
	int32_t		y;

	y = (WINDOW_H - height) * 0.5;
	SDL_SetRenderDrawColor(wolf->sdl.rend, color_base.r, color_base.g,
	color_base.b, 0xFF);
	SDL_RenderDrawLine(wolf->sdl.rend, x, MAX(0, y), x,
	MIN(WINDOW_H, y + height));
	height *= square_size;
	y = (WINDOW_H - height) * 0.5;
	SDL_SetRenderDrawColor(wolf->sdl.rend, color_square.r, color_square.g,
	color_square.b, 0xFF);
	SDL_RenderDrawLine(wolf->sdl.rend, x, y, x, y + height);
}

static void		draw_standard(t_wolf *wolf,
								double height,
								t_color color_base,
								uint16_t x)
{
	uint16_t	y;

	SDL_SetRenderDrawColor(wolf->sdl.rend, color_base.r, color_base.g,
	color_base.b, 0xFF);
	if (height >= WINDOW_H)
		SDL_RenderDrawLine(wolf->sdl.rend, x, 0, x, WINDOW_H);
	else
	{
		y = (WINDOW_H - height) / 2;
		SDL_RenderDrawLine(wolf->sdl.rend, x, y, x, y + height);
	}
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_line_square(t_wolf *wolf,
									double dist,
									uint16_t x,
									double wall_precise)
{
	double		height;
	t_color		color_base;
	t_color		color_square;
	uint16_t	min_y;
	double		square_size;

	get_color(&color_base, 3, 1);
	get_color(&color_square, 5, 1);
	height = WINDOW_H / dist;
	min_y = ABS((WINDOW_H - height) * 0.5);
	square_size = 0.1 + 0.15 - ((double)(wolf->frame % (ANIM_MAX / 2))
	/ (ANIM_MAX / 2)) * 0.15;
	if (wall_precise >= (1 - square_size) / 2
	&& wall_precise <= 1 - (1 - square_size) / 2)
		draw_squared(wolf, height, color_base, color_square, x, square_size);
	else
		draw_standard(wolf, height, color_base, x);
}
