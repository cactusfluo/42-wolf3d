#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_line_end(t_wolf *wolf, double dist, uint16_t x)
{
	double		height;
	int32_t		y;

	height = WINDOW_H / dist;
	y = (WINDOW_H - height) * 0.5;
	SDL_SetRenderDrawColor(wolf->sdl.rend, 0, 0, 0, 0xFF);
	SDL_RenderDrawLine(wolf->sdl.rend, x, MAX(0, y), x,
	MIN(WINDOW_H, y + height));
	height *= 0.5;
	y = (WINDOW_H - height) * 0.5;
	SDL_SetRenderDrawColor(wolf->sdl.rend, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderDrawLine(wolf->sdl.rend, x, y, x, y + height);
}
