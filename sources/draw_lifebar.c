#include "wolf.h"

#define		BLOCK_START		0
#define		BLOCK_MIDDLE	1
#define		BLOCK_END		2

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		draw_block(t_wolf *wolf, uint8_t type, uint16_t x)
{
	SDL_Rect	img_pos;
	SDL_Rect	img_part;

	img_pos.x = x;
	img_pos.y = LIFEBAR_Y;
	img_pos.w = LIFEBAR_UNIT_W;
	img_pos.h = 13;
	img_part.x = type * LIFEBAR_UNIT_W;
	img_part.y = 0;
	img_part.w = LIFEBAR_UNIT_W;
	img_part.h = 13;
	SDL_RenderCopy(wolf->sdl.rend, wolf->images.lifebar, &img_part, &img_pos);
}

static void		draw_main_even(t_wolf *wolf, uint8_t to_draw, uint16_t offset)
{
	draw_block(wolf, BLOCK_MIDDLE, LIFEBAR_X - offset);
	draw_block(wolf, BLOCK_MIDDLE, LIFEBAR_X + offset);
	if (to_draw > 0)
		draw_main_even(wolf, to_draw - 2, offset + LIFEBAR_UNIT_W);
}

static void		draw_main_odd(t_wolf *wolf, uint8_t to_draw)
{
	draw_block(wolf, BLOCK_MIDDLE, LIFEBAR_X);
	if (to_draw > 0)
		draw_main_even(wolf, to_draw - 1, 5);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_lifebar(t_wolf *wolf)
{
	uint16_t	x_min;
	uint16_t	x_max;
	uint8_t		block_number;

	x_min = 0;
	x_max = 0;
	block_number =
	((wolf->life / GAME_LIFE_DEFAULT) * LIFEBAR_W) / LIFEBAR_UNIT_W;
	if (block_number & 1)
	{
		draw_main_odd(wolf, block_number);
		draw_block(wolf, BLOCK_START,
		LIFEBAR_X - (block_number / 2) * LIFEBAR_UNIT_W - 15);
		draw_block(wolf, BLOCK_END,
		LIFEBAR_X + (block_number / 2) * LIFEBAR_UNIT_W + 15);
	}
	else
	{
		draw_main_even(wolf, block_number, 0);
		draw_block(wolf, BLOCK_START,
		LIFEBAR_X - (block_number / 2) * LIFEBAR_UNIT_W - LIFEBAR_UNIT_W);
		draw_block(wolf, BLOCK_END,
		LIFEBAR_X + (block_number / 2) * LIFEBAR_UNIT_W + LIFEBAR_UNIT_W);
	}
}
