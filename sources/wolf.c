#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		init_keys(t_wolf *wolf)
{
	wolf->keys.up = 0;
	wolf->keys.down = 0;
	wolf->keys.left = 0;
	wolf->keys.right = 0;
	wolf->keys.enter = 1;
	wolf->keys.space = 0;
}

static void		init_player(t_wolf *wolf)
{
	wolf->pos.x = 0;
	wolf->pos.y = 0;
	wolf->angle = 0;
	wolf->angle_acc = 0;
	wolf->cam_width = DEFAULT_CAM_WIDTH;
	wolf->acc.x = 0;
	wolf->acc.y = 0;
	wolf->life = GAME_LIFE_DEFAULT;
	wolf->power_charge = 0;
	wolf->time = 0;
}

static void		init_images(t_wolf *wolf)
{
	wolf->images.lifebar = load_image(wolf, PATH_LIFEBAR);
	if (!wolf->images.lifebar)
		ct_exit_fail("Cannot load lifebar image.");
	wolf->images.powerbar = load_image(wolf, PATH_POWERBAR);
	if (!wolf->images.powerbar)
		ct_exit_fail("Cannot load powerbar image.");
	wolf->images.powerbarmax = load_image(wolf, PATH_POWERBARMAX);
	if (!wolf->images.powerbarmax)
		ct_exit_fail("Cannot load powerbar image.");
	wolf->images.digits = load_image(wolf, PATH_DIGITS);
	if (!wolf->images.digits)
		ct_exit_fail("Cannot load digits image.");
	wolf->images.panel = load_image(wolf, PATH_PANEL);
	if (!wolf->images.panel)
		ct_exit_fail("Cannot load panel image.");
	wolf->images.spaceship = load_image(wolf, PATH_SPACESHIP);
	if (!wolf->images.spaceship)
		ct_exit_fail("Cannot load spaceship image.");
	wolf->images.alpha = load_image(wolf, PATH_ALPHABET);
	if (!wolf->images.alpha)
		ct_exit_fail("Cannot load alphabet image.");
	wolf->images.cursor = load_image(wolf, PATH_CURSOR);
	if (!wolf->images.cursor)
		ct_exit_fail("Cannot load cursor image.");
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

int32_t		main(int32_t ac, char **av)
{
	t_wolf			wolf;

	init_player(&wolf);
	init_keys(&wolf);
	wolf.frame = 0;
	if (ac > 2)
		ct_exit_fail("Wrong number of arguments.");
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		ct_exit_fail("Cannot initialize SDL.");
	if (!(wolf.sdl.win = SDL_CreateWindow("Kawai-Manatee-3D",
	SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOW_W, WINDOW_H,
	SDL_WINDOW_SHOWN)))
		ct_exit_fail("Cannot initialize SDL window.");
	if (!(wolf.sdl.rend = SDL_CreateRenderer(wolf.sdl.win, -1,
	SDL_RENDERER_ACCELERATED)))
		ct_exit_fail("Cannot initialize SDL renderer.");
	load_map(&wolf, av[1]);
	init_images(&wolf);
	SDL_SetRenderDrawColor(wolf.sdl.rend, 0xFF, 0xFF, 0xFF, 0xFF);
	loop(&wolf);
	SDL_DestroyRenderer(wolf.sdl.rend);
	SDL_DestroyWindow(wolf.sdl.win);
	SDL_Quit();
	return (0);
}
