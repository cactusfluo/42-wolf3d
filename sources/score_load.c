#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

static void		check_input(char *buffer)
{
	uint8_t		i;

	i = 0;
	while (i < SCORE_NAME_MAX
	&& (!buffer[i] || ct_isalpha(buffer[i]) || buffer[i] == '-'))
		i++;
	if (i != SCORE_NAME_MAX)
		ct_exit_fail("The saving file is corrupted.");
}

static int8_t	get_new_score_index(t_score *scores, uint32_t new_score)
{
	uint8_t		i;
	uint8_t		worst_index;
	uint32_t	worst_score;

	i = 0;
	while (i < SCORE_MAX_SAVE)
	{
		if (!i || !scores[i].score || scores[i].score > worst_score)
		{
			worst_index = i;
			worst_score = scores[i].score;
			if (!worst_score)
				break ;
		}
		i++;
	}
	if (!worst_score || worst_score >= new_score)
		return (worst_index);
	ct_putendl("Your score is too high to be saved. Try again.");
	exit (0);
}

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void		load_scores(t_wolf *wolf, t_score *scores, uint8_t *new_score_index)
{
	uint8_t		i;
	char		buffer[SCORE_NAME_MAX + 4];
	int32_t		ret;
	int32_t		file;
	char		*save_file_path;

	if (wolf->score == 0)
		ct_exit_fail("Your score cannot be 0.");
	ct_bzero(scores, sizeof(t_score) * SCORE_MAX_SAVE);
	if (!(save_file_path = ct_strjoin(wolf->race_path, "-save")))
		ct_exit_fail("Not enough memory to save player score.");
	if ((file = open(save_file_path, O_RDONLY)) < 0)
		return ;
	i = 0;
	while ((ret = read(file, buffer, SCORE_NAME_MAX + 4)) == SCORE_NAME_MAX + 4)
	{
		check_input(buffer);
		ct_memcpy(scores[i].name, buffer, SCORE_NAME_MAX);
		scores[i].score = *(uint32_t*)(buffer + SCORE_NAME_MAX);
		i++;
	}
	if (ret && ret != SCORE_NAME_MAX + 4)
		ct_exit_fail("The saving file is corrupted.");
	close(file);
	*new_score_index = get_new_score_index(scores, wolf->score);
}
