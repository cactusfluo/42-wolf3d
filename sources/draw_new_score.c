#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			draw_new_score(t_wolf *wolf)
{
	SDL_Rect		img_pos;
	SDL_Rect		img_part;
	uint32_t		x;
	char			*string;

	string = ct_itoa(wolf->score);
	img_part.y = 0;
	img_part.w = 60;
	img_part.h = 60;
	img_pos.y = SCORE_Y;
	img_pos.w = 60;
	img_pos.h = 60;
	x = 0;
	while (string[x])
	{
		img_pos.x = SCORE_X + 30 * x;
		img_part.x = (string[x] - '0') * 60;
		SDL_RenderCopy(wolf->sdl.rend, wolf->images.digits, &img_part,
		&img_pos);
		++x;
	}
	free(string);
}
