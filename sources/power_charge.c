#include "wolf.h"

/*
********************************************************************************
*** STATIC FUNCTIONS ***********************************************************
********************************************************************************
*/

/*
********************************************************************************
*** PUBLIC FUNCTION ************************************************************
********************************************************************************
*/

void			handle_power_charge(t_wolf *wolf, double dt)
{
	double		speed;
	double		diff;

	speed = sqrt(wolf->sacc.x * wolf->sacc.x + wolf->sacc.y * wolf->sacc.y);
	diff = speed - GAME_MAX_SPEED + 10;
	if (diff >= 0)
		wolf->power_charge = MIN(GAME_POWER_MAX,
		wolf->power_charge + diff * GAME_POWER_COEF * dt);
}
